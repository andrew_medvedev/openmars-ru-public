'use strict';

var mongoose = require('mongoose'),
	log = require('winston'),
	async = require('async');
var schemas = require('./schemas.js'),
	utils = require('./utils/odmUtils.js');

function connect(callback) {
	var dbUrl = appSettings.db.url;
	var dbUrlSanitized = utils.sanitizeDbUrlWithCredentials(dbUrl);

	console.log('dbUrl', dbUrl);

	mongoose.connect(dbUrl, { server: { auto_reconnect: true } });

	mongoose.connection.once('connected', () => {
		log.info('Mongoose connected on %s ', dbUrlSanitized);
		if(callback){
			callback(null);
			callback = null;
		}
		mongoose.connection.on('connected', error => {
			log.info('Mongoose connected on %s ', dbUrlSanitized);
		});
	});
	mongoose.connection.once('error', error => {
		log.info('Mongoose connection error %j ', error, {});
		if(callback){
			callback(error);
			callback = null;
		}
		mongoose.disconnect();
		mongoose.connection.on('error', error => {
			log.info('Mongoose connection error %j ', error, {});
			mongoose.disconnect();
		});
	});
	mongoose.connection.on('reconnected', () => {
		log.info('Mongoose reconnected on %s ', dbUrlSanitized);
	});
	mongoose.connection.on('disconnected', () => {
		log.info('Mongoose disconnected from %s ', dbUrlSanitized);

		log.info('Trying to reconnect to %s AFTER $d MS...', dbUrlSanitized, appSettings.db.reconnectionThreshold);
		setTimeout(() => {
			log.info('Trying to reconnect to %s...', dbUrlSanitized);
			mongoose.connect(dbUrl, { server: { auto_reconnect: true } });
		}, appSettings.db.reconnectionThreshold);
	});
}

function closeConnection(callback) {
	log.info('Trying to close Mongoose connection');
	mongoose.connection.close(callback);
}

const Rover = mongoose.model('Rover', schemas.RoverSchema);
const RoverCamera = mongoose.model('RoverCamera', schemas.RoverCameraSchema);
const RoverCameraPhoto = mongoose.model('RoverCameraPhoto', schemas.RoverCameraPhotoSchema);

function cleanUp(callback) {
	log.info('Trying to clean up all Mongoose collections...');

	var callbackFn = error => {
		if(error){
			log.info('Trying to clean up all Mongoose collections... ERROR %j ', error, {});
		} else {
			log.info('Trying to clean up all Mongoose collections... SUCCESS');
		}
		callback(error);
	}

	async.parallel([
		callback => {
			log.info('Trying to clean up BannerShow collection!')
			BannerShow.remove({}, callback);
		},
		callback => {
			log.info('Trying to clean up BannerClick collection!')
			BannerClick.remove({}, callback);
		}
	], callbackFn);
}

function closeConnection(callback) {
	log.info('Trying to close Mongoose connection');

	var callbackFn = error => {
		if(error){
			callback(error);
		} else {
			callback(null);
		}
	}

	mongoose.connection.removeAllListeners('disconnected');
	mongoose.connection.removeAllListeners('error');
	mongoose.connection.removeAllListeners('reconnected');
	mongoose.connection.removeAllListeners('connected');
	mongoose.connection.once('disconnected', () => {
		log.info('Trying to close Mongoose connection... OK');
	});

	mongoose.connection.close(callbackFn);
}

module.exports = {
	connect,
	cleanUp,
	closeConnection,
	Rover,
	RoverCamera,
	RoverCameraPhoto
};