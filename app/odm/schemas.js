'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var RoverSchema = new Schema({
	roverId: {
		type: Number,
		unique: true
	},
	name: {
		type: String,
		unique: true
	},
	landingDate: Date,
	maxSols: Number,
	lastDate: Date,
	totalAmountOfPhotos: Number,
	dateFetched: Date
});
RoverSchema.set('autoIndex', false);

var RoverCameraSchema = new Schema({
	cameraId: {
		type: Number,
		unique: true
	},
	shortName: String,
	fullName: String,
	roverId: String,	// => RoverSchema
	dateFetched: Date
});
RoverCameraSchema.set('autoIndex', false);

var RoverCameraPhotoSchema = new Schema({
	photoId: Number,
	sol: Number,
	roverCameraSource: String,	// => RoverCameraSchema
	imgSourceUrl: String,
	earthDateTaken: Date,
	dateFetched: Date
});

module.exports = {
	RoverSchema,
	RoverCameraSchema,
	RoverCameraPhotoSchema
};