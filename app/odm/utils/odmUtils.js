'use strict';

module.exports = {
	sanitizeDbUrlWithCredentials
};

var _ = require('underscore');

function sanitizeDbUrlWithCredentials(url) {
	var prt1 = url.split('@');
	if(prt1.length !== 2){
		return url;
	}
	var prt2 = prt1[0].split('//');
	if(prt2.length !== 2){
		return null;
	}
	var prt3 = prt2[0].split(':');
	if(prt3.length !== 2){
		return null;
	}
	var from = prt2[0].length + 2 + prt3[0].length + 1;
	var count = prt3[1].length;

	var astriks = '';
	_.times(count, () => astriks += '*');

	return prt2[0].concat('//', prt3[0], ':', astriks, '@', prt1[1]);
}