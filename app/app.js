'use strict';

module.exports = {
	runApp,
	shutdownApp
};

var winston = require('winston'),
	log = winston;
var http = require('http'),
	express = require('express'),
	app = express(),
	async = require('async');

var server;

function configureProductionWinston() {
	log.add(winston.transports.File, {
		filename: 'app.log'
	});
	log.remove(winston.transports.Console);
}

function runApp(callback) {
	if(!appSettings.isDebug){
		configureProductionWinston();
	}

	require('./utils/underscoreUtils.js');

	app.use(express.static(appSettings.webServer.staticFiles));

	async.series([
		callbackFn => initODM(callbackFn),
		callbackFn => initWebServer(callbackFn)
	], callback);
}

function initODM(callback) {
	var connectionCallback = function(error) {
		if(error){
			callback(error);
		} else {
			require('./api/jobsImpl.js').initWithModels();

			callback(null);
		}
	};

	require('./odm/odm.js').connect(connectionCallback);
}

function initWebServer(callback) {
	require('./routing.js').initRouting(app);

	server = http.createServer(app).listen(appSettings.webServer.port, appSettings.webServer.host);
	server.on('listening', () => {
		log.info('Running on %s:%s', appSettings.webServer.host, appSettings.webServer.port);
		callback();
	});
}

function shutdownApp() {
	server.close();
}