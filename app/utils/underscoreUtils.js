'use strict';

var _ = require('underscore');

_.sanitObjectId = function(object, sourceField, destField) {
    if(destField){
        object[destField] = object[sourceField].toString();
        delete object[sourceField];
    } else {
        object[sourceField] = object[sourceField].toString();
    }
};
_.modelToObject = function(model) {
	if(_.isNull(model)){
		return null;
	}
	
    var out = model.toObject();
    
    delete out.__v;
    if(!_.isUndefined(out._id)){
        _.sanitObjectId(out, '_id', 'id');
    }

    return out;
};
_.arrayOfModelsToObjects = function(models) {
    var out = [];

    _.each(models, function(model) {
        out[out.length] = _.modelToObject(model);
    });

    return out;
};