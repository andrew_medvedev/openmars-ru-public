'use strict';

module.exports = {
	getRovers,
	getRover,
	getPhotos
};

var _ = require('underscore');
var NodeCache = require('node-cache');
var jobsImpl = require('./jobsImpl.js');

var jobsCaching = new NodeCache({ stdTTL: 60 * 60 });
const GET_ROVERS_CACHE_KEY = 'get-rovers';
const GET_ROVER_CACHE_KEY = 'get-rover';
const GET_PHOTOS_CACHE_KEY = 'get-photos';

function getRovers(callback) {
	var out = jobsCaching.get(GET_ROVERS_CACHE_KEY);

	if(!_.isUndefined(out)){
		callback(null, out);
	} else {
		var callbackFn = (err, result) => {
			if(err){
				callback(err, null);
			} else {
				jobsCaching.set(GET_ROVERS_CACHE_KEY, result);
				callback(null, result);
			}
		}

		jobsImpl.getRovers(callbackFn);
	}
}
function getRover(roverId, callback) {
	var out = jobsCaching.get(GET_ROVER_CACHE_KEY + '-' + roverId);
	
	if(!_.isUndefined(out)){
		callback(null, out);
	} else {
		var callbackFn = (err, result) => {
			if(err){
				callback(err, null);
			} else {
				jobsCaching.set(GET_ROVER_CACHE_KEY + '-' + roverId, result);
				callback(null, result);
			}
		}

		jobsImpl.getRover(roverId, callbackFn);
	}
}
function getPhotos(roverId, sol, callback) {
	var out = jobsCaching.get(GET_PHOTOS_CACHE_KEY + '-' + roverId + '-' + sol);
	
	if(!_.isUndefined(out)){
		callback(null, out);
	} else {
		var callbackFn = (err, result) => {
			if(err){
				callback(err, null);
			} else {
				jobsCaching.set(GET_PHOTOS_CACHE_KEY + '-' + roverId + '-' + sol, result);
				callback(null, result);
			}
		}

		jobsImpl.getPhotos(roverId, sol, callbackFn);
	}
}