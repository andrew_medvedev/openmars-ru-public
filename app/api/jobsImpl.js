'use strict';

module.exports = {
	initWithModels,
	getRovers,
	getRover,
	getPhotos
};

var _ = require('underscore');

var Rover;
var RoverCamera;
var RoverCameraPhoto;

function initWithModels() {
	var odm = require('../odm/odm.js');

	Rover = odm.Rover;
	RoverCamera = odm.RoverCamera;
	RoverCameraPhoto = odm.RoverCameraPhoto;
}

function getRovers(callback) {
	var callbackFn = (error, result) => {
		if(error){
			callback(error, null);
		} else {
			callback(null, _.arrayOfModelsToObjects(result));
		}
	}

	Rover
		.find()
		.select('-roverId -landingDate -maxSols -lastDate -totalAmountOfPhotos -dateFetched')
		.exec(callbackFn);
}
function getRover(roverId, callback) {
	function getRover() {
		var callbackFn = (error, result) => {
			if(error){
				callback(error, null);
			} else {
				getRoverCameras(result);
			}
		}

		Rover
			.findById(roverId)
			.select('-dateFetched')
			.exec(callbackFn);
	}
	function getRoverCameras(rover) {
		var callbackFn = (error, result) => {
			if(error){
				callback(error, null);
			} else {
				var camIds = [];
				_.each(result, cam => {
					camIds.push(cam.id);
				})

				getAmountOfRoverPhotos(_.extend(_.modelToObject(rover), { 
					cameras: _.arrayOfModelsToObjects(result)
				}), camIds);
			}
		}

		RoverCamera
			.find({ roverId: rover.id })
			.select('-roverId -dateFetched -cameraId')
			.exec(callbackFn);
	}
	function getAmountOfRoverPhotos(rover, camIds) {
		var callbackFn = (err, count) => {
			if(err){
				callback(err, null);
			} else {
				callback(null, _.extend(rover, { photosInDb: count }));
			}
		}

		RoverCameraPhoto
			.count({ roverCameraSource: { '$in': camIds } })
			.exec(callbackFn);
	}
	
	getRover();
}
function getPhotos(roverId, sol, callback) {
	function getRoverCameras() {
		var callbackFn = (error, result) => {
			if(error){
				callback(error, null);
			} else {
				var camIds = [];
				_.each(result, cam => {
					camIds.push(cam.id);
				});
				getRoverPhotos(camIds);
			}
		}

		RoverCamera
			.find({ roverId })
			.select('-dateFetched -cameraId -shortName -fullName')
			.exec(callbackFn);
	}
	function getRoverPhotos(camIds) {
		var callbackFn = (error, result) => {
			if(error){
				callback(error, null);
			} else {
				callback(null, _.arrayOfModelsToObjects(result));
			}
		}

		RoverCameraPhoto
			.find({ roverCameraSource: { '$in': camIds }, sol })
			.select('-_id -photoId -sol -dateFetched')
			.exec(callbackFn);
	}

	getRoverCameras();
}