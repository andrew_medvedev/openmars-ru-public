'use strict';

module.exports = {
	getRovers,
	getRover,
	getPhotos
};

var _ = require('underscore'),
	jobs = require('./jobs.js');

function getRovers(req, res) {
	var callbackFn = (error, rovers) => {
		if(error){
			res.send({ error: error.message, rovers: null });
		} else {
			res.send({ error: null, rovers });
		}
	}

	jobs.getRovers(callbackFn);
}
function getRover(req, res) {
	var roverId = req.query.roverId;
	var callbackFn = (error, rover) => {
		if(error){
			res.send({ error: error.message, rover: null });
		} else {
			res.send({ error: null, rover });
		}
	}

	jobs.getRover(roverId, callbackFn);
}
function getPhotos(req, res) {
	var roverId = req.query.roverId;
	var sol = Number(req.query.sol);
	if(_.isNaN(sol)){
		res.status(400).end();
	} else {
		var callbackFn = (error, photos) => {
			if(error){
				res.send({ error: error.message, photos: null });
			} else {
				res.send({ error: null, photos });
			}
		}

		jobs.getPhotos(roverId, sol, callbackFn);
	}
}