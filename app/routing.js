'use strict';

module.exports = {
	initRouting
};

var api = require('./api/api.js');

function initRouting(app) {
	app.get('/api/getRovers', api.getRovers);
	app.get('/api/getRover', api.getRover);
	app.get('/api/getPhotos', api.getPhotos);
	
}