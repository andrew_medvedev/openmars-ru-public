module.exports = {
	mode: 'debug',
	debugSettings: require('./debugSettings.js'),
	mockingSettings: require('./mockingSettings.js'),
	productionSettings: require('./productionSettings.js')
};