module.exports = {
	db: {
		url: 'mongodb://127.0.0.1:27017/openmars',
		reconnectionThreshold: 5000
	},
	webServer: {
		staticFiles: 'public',
		host: '127.0.0.1',
		port: 1337
	}
};