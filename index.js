'use strict';

var _ = require('underscore'),
	log = require('winston');
var settings = require('./settings.js');
var app = require('./app/app.js');

if(_.include(process.argv, '--production')){
	settings.mode = 'production';
}

switch(settings.mode){
	case 'production':
		global.appSettings = settings.productionSettings;
		global.appSettings.isDebug = false;
		break;
	case 'mocking':
		global.appSettings = settings.mockingSettings;
		global.appSettings.isDebug = true;
	default:
		global.appSettings = settings.debugSettings;
		global.appSettings.isDebug = true;
}

var callbackFn = () => {
	log.info('Trying to run app... OK');
}

log.info('Trying to run app...');
app.runApp(callbackFn);