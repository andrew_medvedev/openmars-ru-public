app.service('ApiService', ['$http', function($http){

	this.getRovers = function(callback) {
		$http.get('/api/getRovers')
			.success(function(resp) {
				var err = resp.error;
				var rovers = resp.rovers;

				if(err){
					callback(err, null);
				} else {
					callback(err, rovers);
				}
			})
			.error(function(err, status) {
				console.error(err, status);
				callback(err, null);
			});
	};

	this.getRover = function(roverId, callback) {
		$http.get('/api/getRover?roverId=' + roverId)
			.success(function(resp) {
				var err = resp.error;
				var rover = resp.rover;

				console.log('rover', rover);

				if(err){
					callback(err, null);
				} else {
					callback(err, rover);
				}
			})
			.error(function(err, status) {
				console.error(err, status);
				callback(err, null);
			});
	};

	this.getPhotos = function(roverId, sol, callback) {
		$http.get('/api/getPhotos?roverId=' + roverId + '&sol=' + sol)
			.success(function(resp) {
				var err = resp.error;
				var photos = resp.photos;

				if(err){
					callback(err, null);
				} else {
					callback(err, photos);
				}
			})
			.error(function(err, status) {
				console.error(err, status);
				callback(err, null);
			});
	};
}]);