app.service('UtilsService', function(){

	this.prepareRoverCard = function(roverCard) {
		var cameras = '';

		_.each(roverCard.cameras, function(cam) {
			cameras += cam.fullName + ' (' + cam.shortName + '), ';
		});

		roverCard.camerasRaw = roverCard.cameras;
		roverCard.camerasAmount = roverCard.cameras.length;
		roverCard.cameras = cameras.slice(0, -2);

		var landingDate = new Date(roverCard.landingDate);

		roverCard.landingDate = landingDate.toISOString().slice(0, 10);

		return roverCard;
	};
});