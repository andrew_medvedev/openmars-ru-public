app.controller('GalleryController', ['$scope', 'ApiService', 'UtilsService', '$routeParams', '$location', '$timeout',
    function($scope, ApiService, UtilsService, $routeParams, $location, $timeout) {
        var aboutPage = false;

        var pickedRover;
        var loading;
        var pickedRoverCard;

        $scope.isAboutPage = function() {
            return aboutPage;
        };
        $scope.isLoading = function() {
            return loading;
        };
        $scope.pickRover = function($rover) {
            if($scope.rovers){
            	_pickRover($rover);
            } else {
            	var callbackFn = function(err, rovers) {
	                if (!err) {
	                    $scope.rovers = {};
	                    _.each(rovers, function(rover) {
	                        $scope.rovers[rover.name] = rover.id;
	                    });

	                    _pickRover($rover);
	                }
	            };

            	ApiService.getRovers(callbackFn);
            }
        };
        $scope.locateToRover = function($rover){
            $location.path('/' + $rover + '/1');
        }
        $scope.pickAboutPage = function() {
            $location.path('/about');
        };
        $scope.isCuriosityPicked = function() {
            return pickedRover === 'Curiosity';
        };
        $scope.isOpportunityPicked = function() {
            return pickedRover === 'Opportunity';
        };
        $scope.isSpiritPicked = function() {
            return pickedRover === 'Spirit';
        };
        $scope.getPickedRover = function() {
            return pickedRover;
        };
        $scope.getPickedRoverCard = function() {
            return pickedRoverCard;
        };
        $scope.getPickedRoverImgPath = function() {
            switch (pickedRover) {
                case 'Curiosity':
                    return 'assets/img/curiosityRover.jpg';
                case 'Opportunity':
                    return 'assets/img/opportunityRover.jpg';
                case 'Spirit':
                    return 'assets/img/spiritRover.jpg';
            }
        };

        function _pickRover(_rover) {
        	aboutPage = false;
            if (!loading && pickedRover !== _rover) {
                loading = true;
                pickedRover = _rover;

                var callbackFn = function(err, rover) {
                    if (err) {
                        console.error(err);
                    } else {
                        pickedRoverCard = UtilsService.prepareRoverCard(rover);

                        loading = false;

                        $timeout(
                        	function() {
                        		$scope.$broadcast('pickRover', { fetchSol: Number($routeParams.sol) });
                        	},
                        	100
                        );
                    }
                };

                ApiService.getRover($scope.rovers[pickedRover], callbackFn);
            }
        }

        $scope.$watch(
        	function() {
        		return $routeParams.rover;
        	},
        	function() {
        		switch($routeParams.rover){
					case 'curiosity':
						$scope.pickRover('Curiosity');
						aboutPage = false;
						break;
					case 'opportunity':
						$scope.pickRover('Opportunity');
						aboutPage = false;
						break;
					case 'spirit':
						$scope.pickRover('Spirit');
						aboutPage = false;
						break;
					case 'about':
						aboutPage = true;
						break;
					default:
						$location.path('/');
						break;
				}
        	}
        );
    }
])