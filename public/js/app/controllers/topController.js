app.controller('TopController', ['$scope', 'ApiService', '$location', '$routeParams',
    function($scope, ApiService, $location, $routeParams) {
        var topLevelPage;
        var isLoading = false;

        $scope.topLevelPage = function() {
            return topLevelPage;
        };
        $scope.open = function() {
            isLoading = true;

            var callbackFn = function(err, rovers) {
                if (!err) {
                    $scope.rovers = {};
                    _.each(rovers, function(rover) {
                        $scope.rovers[rover.name] = rover.id;
                    });
                    topLevelPage = 'main';
                    $location.path('/curiosity/1');
                }
                
                isLoading = false;
            };

            ApiService.getRovers(callbackFn);
        };
        $scope.isOpenDisabled = function() {
            return isLoading;
        };

        $scope.$watch(
        	function() {
        		return $routeParams.rover;
        	},
        	function() {
        		if(_.isUndefined($routeParams.rover)){
        			topLevelPage = 'starting';
        		} else {
        			topLevelPage = 'main';
        		}
        	}
        )
    }
]);