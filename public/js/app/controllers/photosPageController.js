app.controller('PhotosPageController', ['$scope', 'ApiService', '$location', function($scope, ApiService, $location){
	var currentSol;
	var currentPhotos;
	var fetchingNow = false;

	$scope.getCurrentSol = function() {
		return currentSol;
	};
	$scope.fetchSol = function($sol) {
		if(!_.isNull($scope.getPickedRoverCard()) && $sol !== currentSol){
			fetchingNow = true;
			currentPhotos = null;
			currentSol = $sol;
			var $roverId = $scope.getPickedRoverCard().id;
			
			var callbackFn = function(err, photos) {
				if(err){
					console.error(err);
				} else if(!_.isNull($scope.getPickedRoverCard()) && $roverId === $scope.getPickedRoverCard().id && currentSol === $sol){
					var lineCounter = 0;
					var lineArray = [];
					var _currentPhotos = [];
					_.each(photos, function(photo) {
						if(++lineCounter <= 4){
							lineArray.push(photo);
						} else {
							_currentPhotos.push(lineArray);
							lineArray = [];
							lineCounter = 0;
						}
					});
					if(lineArray.length > 0){
						_currentPhotos.push(lineArray);
					}
					currentPhotos = _currentPhotos;
				}

				fetchingNow = false;
			};

			ApiService.getPhotos($roverId, currentSol, callbackFn);
		}
	};
	$scope.isShowPhotos = function() {
		return !_.isNull(currentPhotos);
	};
	$scope.getCurrentPhotos = function() {
		return currentPhotos;
	};
	$scope.haveCurrentPhotos = function() {
		return !_.isUndefined(currentPhotos) && !_.isNull(currentPhotos) && currentPhotos.length > 0;
	};
	$scope.getPhotosPlaceholder = function() {
		return fetchingNow ? 'Грузим...' : 'Здесь нет фото, марсоход отдыхал :)';
	};
	$scope.clickOnPhoto = function($event) {
		_clickOnPhoto($event);
	};
	$scope.paginatorGetPage = function() {
		if(_.isUndefined(currentSol)){
			return 1;
		} else {
			return currentSol + 1;
		}
	};
	$scope.paginatorGetPageSize = function() {
		return 1;
	};
	$scope.paginatorGetTotalSize = function() {
		if($scope.getPickedRoverCard()){
			return $scope.getPickedRoverCard().maxSols;
		}

		return 0;
	};
	$scope.paginatorPickPage = function($page) {
		$location.path('/' + $scope.getPickedRover().toLowerCase() + '/' + $page);
	};

	function _clickOnPhoto($event) {
		var $lightbox = $('#lightbox');

		var $img = $($event.target),
			src = $img.attr('src'),
			alt = $img.attr('alt'),
			css = {
				'maxWidth': $(window).width() - 100,
				'maxHeight': $(window).height() - 100
			};

		$lightbox.find('.close').addClass('hidden');
		$lightbox.find('img').attr('src', src);
		$lightbox.find('img').attr('alt', alt);
		$lightbox.find('img').css(css);

		var $photo = getTargetPhoto(src);
		var $camera = null;
		if($photo){
			$camera = findCamera($scope.getPickedRoverCard().camerasRaw, $photo.roverCameraSource);

			$lightbox.find('span#lightbox-sol').text((currentSol + 1) + "");
			$lightbox.find('span#lightbox-camera').text($camera.fullName + ' (' + $camera.shortName + ')');
			var lightboxDate = new Date($photo.earthDateTaken);
			$lightbox.find('span#lightbox-date').text(lightboxDate.toISOString().slice(0, 10));
		}

		$lightbox.on('shown.bs.modal', function (e) {
			var $img = $lightbox.find('img');

			$lightbox.find('.modal-dialog').css({'width': $img.width()});
			$lightbox.find('.close').removeClass('hidden');
		});
	}

	function getTargetPhoto(imgSourceUrl) {
		for(var i = 0 ; i < currentPhotos.length ; i++){
			for(var j = 0 ; j < currentPhotos[i].length ; j++){
				if(currentPhotos[i][j].imgSourceUrl === imgSourceUrl){
					return currentPhotos[i][j];
				}
			}
		}

		return null;
	}
	function findCamera(rawCameras, targetCameraId) {
		for(var i = 0 ; i < rawCameras.length ; i++){
			if(rawCameras[i].id === targetCameraId){
				return rawCameras[i];
			}
		}

		return null;
	}

	$scope.$on('pickRover', function(ev, args) {
		var _fetchSol = args.fetchSol - 1;
		if(_.isNaN(_fetchSol) || _fetchSol < 0){
			_fetchSol = 0;
		}

		$scope.fetchSol(_fetchSol);
	});
}]);