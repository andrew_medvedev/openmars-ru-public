app.config(['$routeProvider',
    function($routeProvider) {
    	$routeProvider
    		.when('/', {
				templateUrl: '/parts/startingPart.html'
			})
			.when('/:rover', {
				templateUrl: '/parts/mainPart.html',
				controller: 'GalleryController'
			})
			.when('/:rover/:sol', {
				templateUrl: '/parts/mainPart.html',
				controller: 'GalleryController'
			})
			.otherwise({
				redirectTo: '/'
			});
    }
]);