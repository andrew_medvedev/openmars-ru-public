'use strict';

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var winston = require('winston'),
	log = winston,
	_ = require('underscore'),
	request = require('request'),
	async = require('async'),
	clui = require('clui'),
	Progress = clui.Progress;

const dbUrl = 'mongodb://localhost:27017/openmars';
const API_KEY = 'RQmLAIITGIQz8pVtRqLNWmIoLx03mXuapngJgTLo';
const API_VERSION = 'v1';
const ROVERS = ['curiosity', 'apportunity', 'spirit'];
const cameras = ['all', 'fhaz', 'rhaz', 'mast', 'chemcam', 'mahli', 'mardi', 'navcam', 'pancam', 'minites'];

var fromSol;
var toSol;
var ROVER;
initArguments();

configureWinston();

configureMongoose(() => doJob());

var progressSrc;
var progressDest;
var thisProgressBar = new Progress(20);

function initArguments() {
	for(let i = 0 ; i < process.argv.length ; i++){
		var arg = process.argv[i];
		switch(arg){
			case 'from':
				if(process.argv[i + 1]){
					fromSol = Number(process.argv[++i]);
				}
				break;
			case 'to':
				if(process.argv[i + 1]){
					toSol = Number(process.argv[++i]);
				}
				break;
			case 'rover':
				if(process.argv[i + 1]){
					ROVER = process.argv[++i];
				}
				break;
		}
	}

	if(_.isNaN(fromSol) || _.isNaN(toSol)){
		fromSol = 0;
		toSol = 0;
	}
	progressSrc = fromSol;
	progressDest = toSol - fromSol + 1;

	console.log('Fetching from', fromSol, 'to', toSol, 'for rover', ROVER);
}

function configureMongoose(callback) {
	log.info('Trying to connect to MongoDB...');

	mongoose.connect(dbUrl, { db: { forceServerObjectId: true } });
	mongoose.connection.once('connected', () => {
		log.info('Trying to connect to MongoDB... OK');

		callback();
	});
	mongoose.connection.once('error', err => {
		log.info('Some error occurs on mongoose connection %j', err, {});
	});
}

function configureWinston() {
	winston.add(winston.transports.File, {
		filename: 'app.log'
	});
	winston.remove(winston.transports.Console);
}

function doJob() {
	var RoverSchema, RoverCameraSchema, RoverCameraPhotoSchema;
	var Rover, RoverCamera, RoverCameraPhoto;

	var curiosityCached;
	var apportunityCached;
	var spiritCached;

	function initSchemas() {
		RoverSchema = new Schema({
			roverId: {
				type: Number,
				unique: true
			},
			name: {
				type: String,
				unique: true
			},
			landingDate: Date,
			maxSols: Number,
			lastDate: Date,
			totalAmountOfPhotos: Number,
			dateFetched: Date
		});
		RoverSchema.set('autoIndex', false);

		RoverCameraSchema = new Schema({
			cameraId: {
				type: Number,
				unique: true
			},
			shortName: String,
			fullName: String,
			roverId: String,	// => RoverSchema
			dateFetched: Date
		});
		RoverCameraSchema.set('autoIndex', false);

		RoverCameraPhotoSchema = new Schema({
			photoId: Number,
			sol: Number,
			roverCameraSource: String,	// => RoverCameraSchema
			imgSourceUrl: String,
			earthDateTaken: Date,
			dateFetched: Date
		});
	}
	function initModels() {
		Rover = mongoose.model('Rover', RoverSchema);
		RoverCamera = mongoose.model('RoverCamera', RoverCameraSchema);
		RoverCameraPhoto = mongoose.model('RoverCameraPhoto', RoverCameraPhotoSchema);
	}
	function manualEnsureIndexes(callback) {
		log.info('Mongoose ensuring indexes...');
		async.parallel([
			callbackFn => {
				log.info('Mongoose ensuring indexes into Rover...');
				var cb1 = (error, result) => {
					if(error){
						log.info('Mongoose ensuring indexes into Rover... ERROR');
						callbackFn(error);
					} else if(_.isNull(result)){
						Rover.ensureIndexes(cb2);
					} else {
						log.info('Mongoose ensuring indexes into Rover... NO NEED');
						callbackFn(null);
					}
				}
				var cb2 = error => {
					log.info('Mongoose ensuring indexes into Rover... OK');

					callbackFn(null);
				}

				Rover
				.findOne()
				.select('-roverId -name -landingDate -maxSols -lastDate -totalAmountOfPhotos -dateFetched')
				.exec(cb1);
			},
			callbackFn => {
				log.info('Mongoose ensuring indexes into RoverCamera...');
				var cb1 = (error, result) => {
					if(error){
						log.info('Mongoose ensuring indexes into RoverCamera... ERROR');
						callbackFn(error);
					} else if(_.isNull(result)){
						RoverCamera.ensureIndexes(cb2);
					} else {
						log.info('Mongoose ensuring indexes into RoverCamera... NO NEED');
						callbackFn(null);
					}
				}
				var cb2 = error => {
					log.info('Mongoose ensuring indexes into RoverCamera... OK');

					callbackFn(null);
				}

				RoverCamera
				.findOne()
				.select('-cameraId -shortName -fullName -roverId -dateFetched')
				.exec(cb1);
			}
		], callback);
	}
	function fetchData(rover, martianDay, untilMartianDay) {
		console.log(thisProgressBar.update(martianDay - progressSrc, progressDest));
		if(martianDay <= untilMartianDay){
			var url = compileApiUrl(API_VERSION, rover, martianDay, API_KEY);

			log.info('Trying to fetch data from Open NASA API...', { url, rover, martianDay });
			request(url, (err, resp, body) => {
				if(err){
					log.info('Trying to fetch data from Open NASA API... ERROR!', { url, rover, martianDay, err });
					shutdownApp();
				} else if(resp.statusCode !== 200){
					switch(resp.statusCode){
						case 400:
							var jsonResp = JSON.parse(body);
							if(_.isEqual(jsonResp, { errors: 'No Photos Found' })){
								log.info('No photos found here, just skipping');
								fetchData(rover, ++martianDay, untilMartianDay);
							}
							break;
						case 429:
							log.info('Trying to fetch data from Open NASA API... TOO MUCH REQUESTS!');
							console.log('Too much requests!');
							shutdownApp();
							break;
						case 500:
							log.info('Trying to fetch data from Open NASA API... INTERNAL API SERVER ERROR!', { url, rover, martianDay, responseStatus: resp.statusCode });
							shutdownApp();
							break;
						default:
							log.info('Trying to fetch data from Open NASA API... ERROR!', { url, rover, martianDay, responseStatus: resp.statusCode });
							shutdownApp();
					}
				} else {
					log.info('Trying to fetch data from Open NASA API... OK', { url, rover, martianDay });
					var jsonResp = JSON.parse(body);

					if(verifyResponse(jsonResp)){
						log.info('Trying to persist fetched data...');
						persistData(jsonResp, err => {
							if(err){
								log.info('Trying to persist fetched data... FAIL', { err });
								shutdownApp();
							} else {
								log.info('Trying to persist fetched data... OK');
								fetchData(rover, ++martianDay, untilMartianDay);
							}
						});
					} else {
						shutdownApp();
					}
				}
			})
			.on('response', function(response) {
				log.info('API Ratelimit', { 
					'X-RateLimit-Limit': response.headers['x-ratelimit-limit'], 
					'X-RateLimit-Remaining': response.headers['x-ratelimit-remaining'] 
				});
			});
		} else {
			log.info('Fetching done!');
			shutdownApp();
		}
	}
	function persistData(APIData, callback) {
		var seriesTasks = [];

		_.each(APIData.photos, photo => {
			seriesTasks.push(serCallback => {
				var wfCallback = err => {
					serCallback(err);
				}

				async.waterfall([
					callbackFn => {
						persistRover(photo.rover, (err, roverId) => callbackFn(err, roverId));
					},
					(roverId, callbackFn) => {
						persistRoverCamera(roverId, photo.camera, (err, roverCameraId) => callbackFn(err, roverCameraId));
					},
					(roverCameraId, callbackFn) => {
						persistRoverCameraPhoto(roverCameraId, photo, err => callbackFn(err));
					},
				], wfCallback);
			});
		});

		async.series(seriesTasks, callback);
	}
	function persistRover(roverData, callback) {
		var upsertRover = {
			name: roverData.name,
			landingDate: new Date(roverData.landing_date),
			maxSols: roverData.max_sol,
			lastDate: new Date(roverData.max_date),
			totalAmountOfPhotos: roverData.total_photos,
			dateFetched: new Date()
		};
		var callbackFn = (error, rover) => {
			if(error){
				callback(error, null);
			} else {
				callback(null, rover.id);
			}
		};

		Rover.findOneAndUpdate({ roverId: roverData.id }, upsertRover, { upsert: true, new: true }, callbackFn);
	}
	function persistRoverCamera(roverId, roverCameraData, callback) {
		if(!_.isNull(roverCameraData)){
			var upsertRoverCamera = {
				shortName: roverCameraData.name,
				fullName: roverCameraData.full_name,
				roverId,
				dateFetched: new Date()
			};
			var callbackFn = (error, roverCamera) => {
				if(error){
					callback(error, null);
				} else {
					callback(null, roverCamera.id);
				}
			};

			RoverCamera.findOneAndUpdate({ cameraId: roverCameraData.id }, upsertRoverCamera, { upsert: true, new: true }, callbackFn);
		} else {
			callback(null, null);
		}
	}
	function persistRoverCameraPhoto(roverCameraId, roverCameraPhotoData, callback) {
		var cameraPhoto = new RoverCameraPhoto({
			photoId: roverCameraPhotoData.id,
			sol: roverCameraPhotoData.sol,
			roverCameraSource: roverCameraId,
			imgSourceUrl: roverCameraPhotoData.img_src,
			earthDateTaken: new Date(roverCameraPhotoData.earth_date),
			dateFetched: new Date()
		});
		var callbackFn = error => {
			if(error){
				callback(error);
			} else {
				log.info('Inserted new rover photo under ID %s', cameraPhoto.id);
				callback(null);
			}
		}

		cameraPhoto.save(callbackFn);
	}

	initSchemas();
	initModels();
	manualEnsureIndexes(err => {
		if(err){
			log.info('Mongoose ensuring indexes... FAIL', { err });
			shutdownApp();
		} else {
			log.info('Mongoose ensuring indexes... OK');
			fetchData(ROVER, fromSol, toSol);	
		}
	});
}

function compileApiUrl(apiVersion, rover, sol, apiKey) {
	var out = 
		'https://api.nasa.gov/mars-photos/api/{API_VERSION}/rovers/{ROVER}/photos?sol={SOL}&api_key={API_KEY}'
			.replace('{API_VERSION}', apiVersion)
			.replace('{ROVER}', rover)
			.replace('{SOL}', sol)
			.replace('{API_KEY}', apiKey);

	return out;
}
function verifyResponse(APIResponse) {
	log.info('Verifying API response...');
	
	if(!_.has(APIResponse, 'photos')){
		log.info('Verifying API response... FAIL', { APIResponse });
		return false;
	}
	if(!_.isArray(APIResponse.photos)){
		log.info('Verifying API response... FAIL', { APIResponse });
		return false;
	}
	if(APIResponse.photos.length > 0){
		for(let i = 0 ; i < APIResponse.photos.length ; i++){
			var photo = APIResponse.photos[i];

			// Top level photo
			if(!_.has(photo, 'id') || 
				!_.has(photo, 'sol') || 
				!_.has(photo, 'camera') || 
				!_.has(photo, 'img_src') || 
				!_.has(photo, 'earth_date') || 
				!_.has(photo, 'rover')){

				log.info('Verifying API response... FAIL', { APIResponse, photo });
				return false;
			}

			// Photo level camera
			if(!_.isNull(photo.camera)){
				if(!_.has(photo.camera, 'id') || !_.has(photo.camera, 'name') || !_.has(photo.camera, 'rover_id') || !_.has(photo.camera, 'full_name')){
					log.info('Verifying API response... FAIL', { APIResponse, photo, photoCamera: photo.camera });
					return false;
				}
			}

			// Photo level rover
			if(!_.has(photo.rover, 'id') ||
				!_.has(photo.rover, 'name') ||
				!_.has(photo.rover, 'landing_date') ||
				!_.has(photo.rover, 'max_sol') ||
				!_.has(photo.rover, 'max_date') ||
				!_.has(photo.rover, 'total_photos') ||
				!_.has(photo.rover, 'cameras') ||
				!_.isArray(photo.rover.cameras)){

				log.info('Verifying API response... FAIL', { APIResponse, photo, photoRover: photo.rover });
				return false;
			} else {
				// Photo rover level cameras
				if(photo.rover.cameras.length > 0){
					for(let i = 0 ; i < photo.rover.cameras.length ; i++){
						var roverCamera = photo.rover.cameras[i];

						if(!_.has(roverCamera, 'name') || !_.has(roverCamera, 'full_name')){
							log.info('Verifying API response... FAIL', { APIResponse, photo, photoRover: photo.rover, roverCamera });
							return false;
						}
					}
				}
			}
		}
	}

	log.info('Verifying API response... OK');

	return true;
}
function shutdownApp() {
	log.info('Trying to shutdown app...');
	var callbackFn = error => {
		log.info('Trying to shutdown app... OK');
	}

	mongoose.connection.close(callbackFn);
}