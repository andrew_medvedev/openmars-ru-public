'use strict';

var expect = require('chai').expect,
	async = require('async'),
	_ = require('underscore');
var app = require('../app/app.js'),
	api = require('../app/api/api.js'),
	odm = require('../app/odm/odm.js');

before('Setting up', () => {
	global.appSettings = require('../settings.js').mockingSettings;
	global.appSettings.isDebug = true;
});
describe('App running', () => {
	it('Should run', done => {
		var callbackFn = () => {
			done();
		}

		app.runApp(callbackFn);
	});
});
describe('App test', () => {
	var roverDbId;
	var roverCamerasDbIds = [];
	var sol0PhotosDbIds = [];
	var sol1PhotosDbIds = [];
	var sol2PhotosDbIds = [];

	describe('Inserting data for testing purposes', () => {
		it('Should insert a rover', done => {
			var testRover = new odm.Rover({
				roverId: 1,
				name: 'Kamikaze',
				landingDate: new Date('11.09.2001'),
				maxSols: 1000,
				lastDate: new Date('31.12.2005'),
				totalAmountOfPhotos: 1337,
				dateFetched: new Date('13.12.1337')
			});
			var callbackFn = err => {
				expect(err, 'err').to.be.null;

				roverDbId = testRover.id;

				done();
			}

			testRover.save(callbackFn);
		});
		it('Should insert first rover camera', done => {
			var testCamera1 = new odm.RoverCamera({
				cameraId: 1,
				shortName: 'FFF',
				fullName: 'SuperCamera',
				roverId: roverDbId,
				dateFetched: new Date('13.12.1337')
			});
			var callbackFn = err => {
				expect(err, 'err').to.be.null;

				roverCamerasDbIds[0] = testCamera1.id;

				done();
			}

			testCamera1.save(callbackFn);
		});
		it('Should insert second rover camera', done => {
			var testCamera2 = new odm.RoverCamera({
				cameraId: 1,
				shortName: 'EEE',
				fullName: 'Sony',
				roverId: roverDbId,
				dateFetched: new Date('13.12.1337')
			});
			var callbackFn = err => {
				expect(err, 'err').to.be.null;

				roverCamerasDbIds[1] = testCamera2.id;

				done();
			}

			testCamera2.save(callbackFn);
		});
		it('Should insert third rover camera', done => {
			var testCamera3 = new odm.RoverCamera({
				cameraId: 1,
				shortName: 'BBB',
				fullName: 'MLGCamera',
				roverId: roverDbId,
				dateFetched: new Date('13.12.1337')
			});
			var callbackFn = err => {
				expect(err, 'err').to.be.null;

				roverCamerasDbIds[2] = testCamera3.id;

				done();
			}

			testCamera3.save(callbackFn);
		});
		it('Should insert 3 first camera photos of 0 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol0PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 1,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 2,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 3,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 first camera photos of 1 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol1PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 4,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 5,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 6,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 first camera photos of 2 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol2PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 7,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 8,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 9,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[0],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 second camera photos of 0 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol0PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 10,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 11,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 12,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 second camera photos of 1 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol1PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 13,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 14,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 15,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 second camera photos of 2 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol2PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 16,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 17,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 18,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[1],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 third camera photos of 0 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol0PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 19,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 20,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 21,
						sol: 0,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 third camera photos of 1 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol1PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 22,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 23,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 24,
						sol: 1,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
		it('Should insert 3 third camera photos of 2 sol', done => {
			var callbackFn = (err, ids) => {
				expect(err, 'err').to.be.null;

				sol2PhotosDbIds = sol0PhotosDbIds.concat(ids);

				done();
			};

			async.parallel([
				callbackFn => {
					var testPhoto1 = new orm.RoverCameraPhoto({
						photoId: 25,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto1.id);

					testPhoto1.save(clbk);					
				},
				callbackFn => {
					var testPhoto2 = new orm.RoverCameraPhoto({
						photoId: 26,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto2.id);

					testPhoto2.save(clbk);					
				},
				callbackFn => {
					var testPhoto3 = new orm.RoverCameraPhoto({
						photoId: 27,
						sol: 2,
						roverCameraSource: roverCamerasDbIds[2],
						imgSourceUrl: 'url',
						earthDateTaken: new Date('6.9.6969'),
						dateFetched: new Date('13.12.1337')
					});
					var clbk = err => callbackFn(err, testPhoto3.id);

					testPhoto3.save(clbk);					
				}
			], callbackFn);
		});
	});
	describe('Testing API', () => {
		it('Should get info about rover - Name, landing date, amount and list of cameras, max sol, photos done, photos is db', done => {
			expect(roverDbId, 'roverDbId').to.not.be.null;

			var mockReq = {
				query: { roverId: roverDbId }
			};
			var callbackFn = (error, result) => {
				expect(error, 'error').to.be.null;
				expect(result, 'result').to.not.be.null;

				expect(result, 'result').to.deep.equal({
					id: roverDbId,
					roverId: 1,
					name: 'Kamikaze',
					landingDate: new Date('11.09.2001'),
					maxSols: 1000,
					lastDate: new Date('31.12.2005'),
					totalAmountOfPhotos: 1337,
					cameras: [{
						fullName: 'SuperCamera',
						shortName: 'FFF'
					}, {
						shortName: 'EEE',
						fullName: 'Sony'
					}, {
						shortName: 'BBB',
						fullName: 'MLGCamera'
					}],
					photosIsDb: 27
				});

				done();
			};
			var mockRes = {
				send: result => {
					callbackFn(result.error, result.result);
				},
				status: status => {
					callbackFn(new Error('Status: ' + status), null);
					return {
						end: () => {}
					};
				}
			};

			api.getRover(mockReq, mockRes);
		});
		it('Should list 0 sol photos (must be 9 units)', done => {
			expect(roverCamerasDbIds, 'roverCamerasDbIds').to.have.length(3);
			expect(sol0PhotosDbIds, 'sol0PhotosDbIds').to.have.length(9);

			var mockReq = {
				query: { rover: 'all', sol: 0 }
			};
			var callbackFn = (err, result) => {
				expect(error, 'error').to.be.null;
				expect(result, 'result').to.not.be.null;

				var expectedResultPhotos = [];

				var ph = 0;
				for(let i = 0 ; i < roverCamerasDbIds.length ; i++){
					for(let j = 0 ; j < 3 ; j++){
						expectedResultPhotos.push({
							id: sol0PhotosDbIds[ph],
							photoId: ph++,
							sol: 0,
							roverCameraSource: roverCamerasDbIds[i],
							imgSourceUrl: 'url',
							earthDateTaken: new Date('6.9.6969'),
							dateFetched: new Date('13.12.1337')
						});
					}
				}

				expect(result, 'result').to.deep.equal({
					sol: 0,
					photos: expectedResultPhotos
				});
			};
			var mockRes = {
				send: result => {
					callbackFn(result.error, result.result);
				},
				status: status => {
					callbackFn(new Error('Status: ' + status), null);
					return {
						end: () => {}
					};
				}
			};

			api.getPhotos(mockReq, mockRes);
		});
		it('Should list 1 sol photos (must be 9 units)', done => {
			expect(roverCamerasDbIds, 'roverCamerasDbIds').to.have.length(3);
			expect(sol1PhotosDbIds, 'sol1PhotosDbIds').to.have.length(9);

			var mockReq = {
				query: { rover: 'all', sol: 1 }
			};
			var callbackFn = (err, result) => {
				expect(error, 'error').to.be.null;
				expect(result, 'result').to.not.be.null;

				var expectedResultPhotos = [];

				var ph = 0;
				for(let i = 0 ; i < roverCamerasDbIds.length ; i++){
					for(let j = 0 ; j < 3 ; j++){
						expectedResultPhotos.push({
							id: sol1PhotosDbIds[ph],
							photoId: (9 + ph++),
							sol: 1,
							roverCameraSource: roverCamerasDbIds[i],
							imgSourceUrl: 'url',
							earthDateTaken: new Date('6.9.6969'),
							dateFetched: new Date('13.12.1337')
						});
					}
				}

				expect(result, 'result').to.deep.equal({
					sol: 1,
					photos: expectedResultPhotos
				});
			};
			var mockRes = {
				send: result => {
					callbackFn(result.error, result.result);
				},
				status: status => {
					callbackFn(new Error('Status: ' + status), null);
					return {
						end: () => {}
					};
				}
			};

			api.getPhotos(mockReq, mockRes);
		});
		it('Should list 2 sol photos (must be 9 units)', done => {
			expect(roverCamerasDbIds, 'roverCamerasDbIds').to.have.length(3);
			expect(sol2PhotosDbIds, 'sol2PhotosDbIds').to.have.length(9);

			var mockReq = {
				query: { rover: 'all', sol: 2 }
			};
			var callbackFn = (err, result) => {
				expect(error, 'error').to.be.null;
				expect(result, 'result').to.not.be.null;

				var expectedResultPhotos = [];

				var ph = 0;
				for(let i = 0 ; i < roverCamerasDbIds.length ; i++){
					for(let j = 0 ; j < 3 ; j++){
						expectedResultPhotos.push({
							id: sol2PhotosDbIds[ph],
							photoId: (18 + ph++),
							sol: 1,
							roverCameraSource: roverCamerasDbIds[i],
							imgSourceUrl: 'url',
							earthDateTaken: new Date('6.9.6969'),
							dateFetched: new Date('13.12.1337')
						});
					}
				}

				expect(result, 'result').to.deep.equal({
					sol: 2,
					photos: expectedResultPhotos
				});
			};
			var mockRes = {
				send: result => {
					callbackFn(result.error, result.result);
				},
				status: status => {
					callbackFn(new Error('Status: ' + status), null);
					return {
						end: () => {}
					};
				}
			};

			api.getPhotos(mockReq, mockRes);
		});
	});
});
describe('App stopping', () => {
	it('Should shutdown app', () => {
		app.shutdownApp();
	});
});